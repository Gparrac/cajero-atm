# Cajero ATM
Programa diseñado en python3 con el objetivo de implementar fundamentos basicos del mismo lenguaje, utilizando estructuras de datos, sentencias, variables compuestas, etc.
## Enunciado
El proyecto consiste en realizar un cajero ATM que permita retirar montos de billetes segun la cantidad sugerida; se desea la menor cantidad posible a la hora de retorna los billetes, y que al finalizar imprima su nuevo saldo en consola con los billetes para el retiro retiro.
## Pasos del programa

 1. Mostrar saldo inicial de la cuenta.
 2. Presentar menu interactivo ( por el momento solo se trabajara la opción de retirar o salir).
 3. Respectivas validaciones antes del proceso de transacción.
 4. Agrupa en un Lista los billetes necesarios para el retiro.
 5. Imprimir mensaje interactivo sobre el estado de la transacción y los billetes a entregar.
### Autor
Gabriel Parra Cortes
20182020031
Telemática 1. 
